﻿
---
categories:
- Projects
title: "Proyecto de ejemplo Arquetipo"
description: "Este es un proyecto de arquetipo de ejemplo"
date: 2022-02-18T13:45:05+01:00
draft: false
typeProjects: 
 - Arquetipo
typeFronts:
 - Angular
tags:
 - Modulos Federados    
gitLink: "https://gitlab.atmira.com/nexum/sirec/cex/sirec-micro-frontend/angular-mf-poc-4"
---

- [1. Atmira Arquitect](#1-atmira-arquitect)

