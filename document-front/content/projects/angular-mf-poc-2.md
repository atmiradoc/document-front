﻿---
categories:
- Projects
title: "angular-mf-poc-2"
description: "POC prueba carga dinamica de módulos federado"
date: 2022-02-18T13:45:05+01:00
draft: false
typeProjects: 
 - POC-Prueba de concepto
typeFronts:
 - Angular  
tags:
 - Modulos Federados   
 - Angular 13
gitLink: "https://gitlab.atmira.com/nexum/sirec/cex/sirec-micro-frontend/angular-mf-poc-2"
---
# angular-mf-poc-2

**Descripcion:**

Partiendo de la POC1 un problema que vemos rapidamente es que hay que declarar en tiempo de construcción los módulos federados que vamos a consumir con las rutas correspondientes donde se encuentra cada uno.

## Caracteristicas

[ ] con angular 13.1.

[ ] MIcro front con module Federation

[ ] Un contenedor con un projectos federado

[ ] Utiliza la funcion de await loadRemoteModule() de federation para importa el modulo.

[ ] Resolvemos el problema de carga dinamica de federados.

![picture 1](/images/ba3d2d2bcbef8147e29117ce4b0b990268b14e702da3e9c6f796479a49aac40c.png)


## Comentarios

En la rama 'fix-variable-mfe'remplazamos la ruta del modulo federado por la variable global de config, ya que la ruta del modulo la obtendremos dinamicamente.

```
const routes: Routes = [
  {
    path: 'mfe1',
    loadChildren: () => loadRemoteModule({
      type: 'module',
      // remplazamos remoteEntry: 'http://localhost:5000/remoteEntry.js' por la variable de config,
      remoteEntry: window.mfeUrl.mfe1,
      exposedModule: './Module'
    }).then(m => m.ModuleOneModule)
  }
];
```

Destacamos que en el main.ts se carga primero la conguracion y depues hacemos el bootstrap utilizando las promesas y esperando a que cargue la configuración.

```
const init = async()=>{
  try {
    const config = await getRemotesConfig()
    await Promise.all([
      loadRemoteEntry({
        type: 'module',
        remoteEntry: config.mfe1
      })
    ])
    await import('./bootstrap')
  } catch (error) {
    console.error(error)
  }
}

init()

```



como usa loadRemoteModule :

```
{
    path: 'mfe1',
    loadChildren: () => loadRemoteModule({
      type: 'module',
      remoteEntry: 'http://localhost:5000/remoteEntry.js',
      exposedModule: './Module'
    }).then(m => m.ModuleOneModule)
  }

```
