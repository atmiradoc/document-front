﻿---
categories:
- Projects
title: "angular-mf-poc-4"
description: "Proyecto de Micrcofronts, prueba exponer y cargar únicamente componentes "
date: 2022-02-18T13:45:05+01:00
draft: false
typeProjects: 
 - POC-Prueba de concepto
typeFronts:
 - Angular  
tags:
 - Modulos Federados   
 - Angular 13
gitLink: "https://gitlab.atmira.com/nexum/sirec/cex/sirec-micro-frontend/angular-mf-poc-4"
---

#  angular-mf-poc-4


**Descripcion:**

Proyecto Sirec angular-mf-poc-4, Proyecto de Micrcofronts, prueba exponer y cargar únicamente componentes .

## Features

[ ] con angular 13.1.

[ ] MIcro front con module Federation

[ ] UN contenedor con dos projectos federados

[ ] Expone componentes como federados

[ ] Utiliza la funcion de await loadRemoteModule() de federation para importa el modulo y despues consumir el componente.

[ ] Utiliza @angular-architects para desplegar los microfronts

## Comments

**angular arcrquitects se encarga de desplegar los server con la configuracion angular json.**

Con esto :

```
   const component = await loadRemoteModule({
        type: 'module',
        remoteEntry: this.remote,
        exposedModule: this.module
      }).then(m => m[this.component]);
```

Cargamos el componente expuesto desde el modulo defedaro

### Attachments
**image.png** (20.19 kb) [download](https://trello.com/1/cards/620e309920a8293b8eb9acc6/attachments/620e36c129448f589c957078/download/image.png)

![image.png](https://trello.com/1/cards/620e309920a8293b8eb9acc6/attachments/620e36c129448f589c957078/download/image.png)

