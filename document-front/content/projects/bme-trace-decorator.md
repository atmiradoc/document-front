﻿---
categories:
- Projects
title: "bme-trace-decorator"
description: "Este decorador ofrece capacidades para debuggear las clases de Angular..."
date: 2022-02-18T13:45:05+01:00
draft: false
typeProjects: 
 - Libreria
typeFronts:
 - Angular  
tags:
 - Debug
 - Angular 12
 - Decorador
gitLink: "https://gitlab.atmira.com/bme-darwin/bme-arquitectura/frontend/bme-trace-decorator.git"
---
# bme-trace-decorator

**Descripcion:**

Este decorador ofrece capacidades para debuggear las clases de Angular..

## Caracteristicas

- con angular 12.
- Libreria de Angular
- Uso de decorador clase.
- Intercepta los hooks de componente de Angular.

## Comentarios

Como declaramos el decorador:

```
export function NgTraceable(
  activate: boolean = false,
): ClassDecorator {
  return function (constructor: any): any {
    // You can add/remove events for your needs

  
    }
  };
}

```

Como lo usamos:

```
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [],
})
@Traceable(false)
export class AppComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    console.log('Hello World');
  }
}

```

El uso de los decoradores nos permite añadir funcionalidad a nuestras clases sin modificarlas directamente.
