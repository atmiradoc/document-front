﻿---
categories:
- Projects
title: "angular-mf-poc-3"
description: "Modulos Federados en proyectos separados"
date: 2022-02-18T13:45:05+01:00
draft: false
typeProjects: 
 - POC-Prueba de concepto
typeFronts:
 - Angular  
tags:
 - Modulos Federados   
 - Angular 13
gitLink: "https://gitlab.atmira.com/nexum/sirec/cex/sirec-micro-frontend/angular-mf-poc-3-shell.git"
---
#  angular-mf-poc-3-shell

**Descripcion:**

Esta POC simplemente prueba que no es necesario montar un monorepo para utilizar la solución de micro frontends con módulos federados. Para que este proyecto funcione debe estar ejecutandose la POC angular-mf-poc-3-mfe1

## Caracteristicas

[ ] con angular 13.1.

[ ] MIcro front con module Federation

[ ] Un contenedor con un projectos federado

[ ] Utiliza la funcion de await loadRemoteModule() de federation para importa el modulo.

[ ] Proyectos separados entre el contenedor y los remotos.


## Comentarios


como usa loadRemoteModule :

```
{
    path: 'mfe1',
    loadChildren: () => loadRemoteModule({
      type: 'module',
      remoteEntry: 'http://localhost:5000/remoteEntry.js',
      exposedModule: './Module'
    }).then(m => m.ModuleOneModule)
  }

```
