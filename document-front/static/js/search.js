function displayResults (results, store) {
    const searchResults = document.getElementById('results')
    if (results.length) {
      let resultList = ''
      // Iterate and build result list elements
      for (const n in results) {
        const item = store[results[n].ref]
        resultList += '<li><p><a href="' + item.url + '">' + item.title + '</a></p>'
        resultList += '<p>' + item.content.substring(0, 150) + '...</p></li>'
      }
      searchResults.innerHTML = resultList
    } else {
      searchResults.innerHTML = 'No results found.'
    }
  }
  
  function getArrayStr(ar){
     return  ar && ar.join(' ')
  }
  
  // Get the query parameter(s)
  const params = new URLSearchParams(window.location.search)
  const query = params.get('query')
  
  // Perform a search if there is a query
  if (query) {
    // Retain the search input in the form when displaying results
    document.getElementById('search-input').setAttribute('value', query)
  
    const idx = lunr(function () {
      this.ref('id')
      this.field('title', {
        boost: 15
      })
      this.field('tags',{
        boost: 10
      })
      this.field('content', {
        boost: 10
      })
      this.field('categories')
      this.field('typefront')
      this.field('typeproject')

  
      for (const key in window.store) {
        const item={
          id: key,
          title: window.store[key].title,
          tags: getArrayStr(window.store[key].tags),
          categories: getArrayStr(window.store[key].categories),
          typefront: getArrayStr(window.store[key].typefront),
          typeproject: getArrayStr(window.store[key].typeproject),
          content: window.store[key].content
        }
        console.log('item',item)
        this.add(item)
      }
    })
  
    // Perform the search
    const results = idx.search(query)
    // Update the list with results
    displayResults(results, window.store)
  }
  
  